'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
const { Migration } = require('@mikro-orm/migrations');

class Migration20220804103922 extends Migration {

  async up() {
    this.addSql('create table `post` (`id` text not null, `title` text not null, `body` text not null, primary key (`id`));');
  }

}
exports.Migration20220804103922 = Migration20220804103922;
