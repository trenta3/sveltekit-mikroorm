import orm from '$lib/database.ts'
import Post from '$lib/entities/Post.ts'

export async function GET() {
    const em = orm.em.fork()
    let posts = await em.find(Post, {})
    return { body: { posts } }
}

export async function POST({ request }) {
    const em = orm.em.fork()
    const form = await request.formData()
    const data = Object.fromEntries(form)
    const post = new Post(data.title, data.body)
    await em.persistAndFlush([post])
    return await GET()
}
