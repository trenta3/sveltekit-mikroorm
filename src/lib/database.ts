import 'reflect-metadata'
import { MikroORM } from '@mikro-orm/core'
import Post from '$lib/entities/Post.ts'

const orm = await MikroORM.init({
    entities: [Post],
    dbName: "data.sqlite3",
    type: "sqlite",
    migrations: {
	emit: "js",
    },
})
// Create and apply all migrations
const migrator = orm.getMigrator()
await migrator.createMigration()
await migrator.up()

// Export the orm as default
export default orm
